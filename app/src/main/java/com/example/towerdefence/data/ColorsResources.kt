package com.example.towerdefence.data

import android.annotation.SuppressLint
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import com.example.towerdefence.R

object ColorsResources {

    @SuppressLint("UseSparseArrays")
    val colorMap = HashMap<String, Int>()


    fun initiate() {
        colorMap["red"] = Color.rgb(255, 0, 0)
        colorMap["pink"] = Color.rgb(252, 140, 252)
        colorMap["light blue"] = Color.rgb(0, 213, 255)
        colorMap["green"] = Color.rgb(23, 174, 7)
        colorMap["violet"] = Color.rgb(85, 7, 174)
        colorMap["yellow"] = Color.rgb(249, 241, 4)



    }

    fun getColor(str : String) : Int? {
        return colorMap[str]
    }
}