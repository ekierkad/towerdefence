package com.example.towerdefence.data

import android.annotation.SuppressLint
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.towerdefence.R

object ImageResources {

    private lateinit var res: Resources
    private const val DEFAULT_BACKGROUND = 0
    private const val VICTORY = 921
    private const val DEFEAT = 922

    @SuppressLint("UseSparseArrays")
    val idImageMap = HashMap<Int, Int>()

    @SuppressLint("UseSparseArrays")
    val imageBitmapMap = HashMap<Int, Bitmap>()

    fun initiate(resources: Resources) {
        this.res = resources
        enterFlatObjects()
        enterObstacles()
        enterAttackers()
        enterBase()
        enterTowers()
        enterProjectiles()
        enterHeroes()
        enterSpecialImages()
        createBitmaps()
    }

    fun getImage(id: Int): Bitmap {
        return imageBitmapMap[id]!!
    }

    fun getDefaultBackground(): Bitmap {
        return imageBitmapMap[DEFAULT_BACKGROUND]!!
    }

    fun getVictoryImage() : Bitmap {
        return imageBitmapMap[VICTORY]!!
    }

    fun getDefeatImage() : Bitmap {
        return imageBitmapMap[DEFEAT]!!
    }

    private fun enterFlatObjects() {
        idImageMap[0] = R.drawable.grass2
        idImageMap[1] = R.drawable.brick
        idImageMap[3] = R.drawable.gravel
    }

    private fun enterObstacles() {
        idImageMap[21] = R.drawable.rock
        idImageMap[22] = R.drawable.tree
    }

    private fun enterAttackers() {
        idImageMap[100] = R.drawable.kbmob
        idImageMap[101] = R.drawable.kbinfantry
        idImageMap[102] = R.drawable.kbmage
        idImageMap[103] = R.drawable.kbcawalery
        idImageMap[104] = R.drawable.kbufo
        idImageMap[105] = R.drawable.kbninja
        idImageMap[106] = R.drawable.kbsamuraiset

        idImageMap[107] = R.drawable.goblin
        idImageMap[108] = R.drawable.android
        idImageMap[109] = R.drawable.tank
        idImageMap[110] = R.drawable.runner
        idImageMap[111] = R.drawable.snowman
        idImageMap[112] = R.drawable.spider
        idImageMap[113] = R.drawable.treant
        idImageMap[114] = R.drawable.phoenix
        idImageMap[115] = R.drawable.alien
        idImageMap[116] = R.drawable.unicorn

        idImageMap[191] = R.drawable.blackdragon
    }

    private fun enterBase() {
        idImageMap[51] = R.drawable.base1
    }

    private fun enterTowers() {
        idImageMap[201] = R.drawable.eyetower
        idImageMap[202] = R.drawable.stonetower
        idImageMap[203] = R.drawable.rook
        idImageMap[204] = R.drawable.bluetower
        idImageMap[205] = R.drawable.firetower
        idImageMap[206] = R.drawable.magetower
        idImageMap[207] = R.drawable.goldtower
        idImageMap[208] = R.drawable.kbforest
        idImageMap[209] = R.drawable.kbheart
        idImageMap[210] = R.drawable.kbglass
        idImageMap[211] = R.drawable.kbice2
        idImageMap[212] = R.drawable.kbjapan
        idImageMap[213] = R.drawable.kblogic
    }

    private fun enterProjectiles() {
        idImageMap[302] = R.drawable.stoneprojectile
        idImageMap[303] = R.drawable.heart
        idImageMap[305] = R.drawable.fireprojectile
        idImageMap[307] = R.drawable.goldprojectile
        idImageMap[308] = R.drawable.forestprojectile
    }

    private fun enterHeroes() {
        idImageMap[401] = R.drawable.warriorhero
        idImageMap[402] = R.drawable.commander
        idImageMap[403] = R.drawable.economyhero
        idImageMap[404] = R.drawable.magehero
        idImageMap[405] = R.drawable.defenderhero
    }

    private fun enterSpecialImages() {
        idImageMap[901] = R.drawable.gold
        idImageMap[902] = R.drawable.hp
        idImageMap[903] = R.drawable.upgrade
        idImageMap[904] = R.drawable.income
        idImageMap[905] = R.drawable.reroll
        idImageMap[910] = R.drawable.confirmation
        idImageMap[911] = R.drawable.increase
        idImageMap[921] = R.drawable.victory
        idImageMap[922] = R.drawable.defeat
    }

    private fun createBitmaps() {
        for ((key, value) in idImageMap) {
            imageBitmapMap[key] = BitmapFactory.decodeResource(res, value)
        }
    }
}