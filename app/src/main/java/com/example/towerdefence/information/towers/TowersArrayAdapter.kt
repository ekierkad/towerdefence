package com.example.towerdefence.information.towers

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.towerdefence.R
import com.example.towerdefence.data.ImageResources
import java.util.*


class TowersArrayAdapter(context: Context, private var data: ArrayList<TowerInformationStorage>) :
    ArrayAdapter<TowerInformationStorage>(context, R.layout.single_tower_list_item, data) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder : TowersViewHolder

        var view = convertView
        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.single_tower_list_item, parent, false)
            holder = TowersViewHolder()
            holder.nameTextView = view.findViewById<TextView>(R.id.nameTextView) as TextView
            holder.damageTextView = view.findViewById<TextView>(R.id.damageTextView) as TextView
            holder.rangeTextView = view.findViewById<TextView>(R.id.rangeTextView) as TextView
            holder.attackSpeedTextView = view.findViewById<TextView>(R.id.attackSpeedTextView) as TextView
            holder.costTextView = view.findViewById<TextView>(R.id.costTextView) as TextView
            holder.imageView = view.findViewById<ImageView>(R.id.imageView) as ImageView
            view.tag = holder
        }
        else{
            holder = view.tag as (TowersViewHolder)
        }

        holder.nameTextView!!.text = data[position].name
        holder.damageTextView!!.text = data[position].damage.toString()
        holder.rangeTextView!!.text = data[position].range.toString()
        holder.attackSpeedTextView!!.text = data[position].attackSpeed.toString()
        holder.costTextView!!.text = data[position].cost.toString()

        holder.imageView!!.setImageBitmap(ImageResources.getImage(data[position].id))


        return view!!
    }


}