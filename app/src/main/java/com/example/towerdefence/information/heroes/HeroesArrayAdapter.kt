package com.example.towerdefence.information.heroes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.towerdefence.R
import com.example.towerdefence.data.ImageResources
import kotlinx.android.synthetic.main.hero_details.*
import java.util.*


class HeroesArrayAdapter(context: Context, private var data: ArrayList<HeroInformationStorage>) : //TODO dane do zmiany
    ArrayAdapter<HeroInformationStorage>(context, R.layout.single_hero_list_item, data) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder : HeroesViewHolder

        var view = convertView
        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.single_hero_list_item, parent, false)
            holder = HeroesViewHolder()
            holder.nameTextView = view.findViewById(R.id.nameTextView) as TextView
            holder.skillsTextView = view.findViewById(R.id.skillsTextView) as TextView
            holder.imageView = view.findViewById(R.id.imageView) as ImageView
            view.tag = holder
        }
        else{
            holder = view.tag as (HeroesViewHolder)
        }

        holder.nameTextView!!.text = data[position].name
        holder.skillsTextView!!.text = data[position].skills

        holder.imageView!!.setImageBitmap(ImageResources.getImage(data[position].id))
        return view!!
    }

}