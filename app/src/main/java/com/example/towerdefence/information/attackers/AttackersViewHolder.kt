package com.example.towerdefence.information.attackers

import android.widget.ImageView
import android.widget.TextView

class AttackersViewHolder {
    var nameTextView : TextView?  = null
    var damageTextView : TextView?  = null
    var healthTextView : TextView?  = null
    var rewardTextView : TextView?  = null
    var moveSpeedTextView : TextView? = null
    var imageView : ImageView?  = null
}