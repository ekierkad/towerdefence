package com.example.towerdefence.information.heroes

import java.io.Serializable

class HeroInformationStorage(val id : Int, val name : String, val skills : String, val description: String) : Serializable