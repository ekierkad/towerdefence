package com.example.towerdefence.information.towers

import android.widget.ImageView
import android.widget.TextView

class TowersViewHolder {
    var nameTextView : TextView?  = null
    var damageTextView : TextView?  = null
    var rangeTextView : TextView?  = null
    var attackSpeedTextView : TextView?  = null
    var costTextView : TextView?  = null
    var imageView : ImageView?  = null

}