package com.example.towerdefence.information.towers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.towerdefence.R
import com.example.towerdefence.data.ImageResources
import kotlinx.android.synthetic.main.tower_details.*
import kotlinx.android.synthetic.main.tower_details.damageTextView
import kotlinx.android.synthetic.main.tower_details.nameTextView
import kotlinx.android.synthetic.main.tower_details.rangeTextView
import kotlinx.android.synthetic.main.tower_details.attackSpeedTextView
import kotlinx.android.synthetic.main.tower_details.costTextView

class TowerDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tower_details)
        val towerInfo =  intent.getSerializableExtra("towerInformation") as TowerInformationStorage
        nameTextView.text = towerInfo.name
        damageTextView.text = towerInfo.damage.toString()
        rangeTextView.text = towerInfo.range.toString()
        attackSpeedTextView.text = towerInfo.attackSpeed.toString()
        descriptionTextView.text = towerInfo.description
        costTextView.text = towerInfo.cost.toString()
        imageView.setImageBitmap(ImageResources.getImage(towerInfo.id))



    }

}
