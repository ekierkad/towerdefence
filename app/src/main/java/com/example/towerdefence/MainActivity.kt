package com.example.towerdefence

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.information.InformationActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ImageResources.initiate(resources)
    }

    fun singlePlayerGameButtonClicked(view: View) {
        val intent = Intent(this, GameModeActivity::class.java)
        startActivity(intent)
    }

    fun gameInfoButtonClicked(view: View) {
        val intent = Intent(this, InformationActivity::class.java)
        startActivity(intent)
    }
}
