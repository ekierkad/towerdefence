package com.example.towerdefence.game.gameobjects.attackers


import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class CavalryAttacker : Attacker() {
    override var id: Int = 103
    override var damage: Int = 10
    override var description: String = "Look at his horse! His horse is amazing"
    override var health: Int = 125
    override var moveSpeed: Float = 0.03f
    override var reward: Int = 8
    override var visibleName: String = "Cavalryman"

    override val image: Bitmap = ImageResources.getImage(id)
}