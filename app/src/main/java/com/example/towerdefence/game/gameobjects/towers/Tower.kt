package com.example.towerdefence.game.gameobjects.towers

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.Log
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.MapDrawable
import com.example.towerdefence.game.gameobjects.attackers.Attacker
import kotlin.math.roundToInt

abstract class Tower : MapDrawable(), BaseTower {

    protected var shooting: Boolean = false
    var shotAttacker: Attacker? = null
    private var rangeRect: Rect = Rect()
    private var rectHigh = Rect()
    private var background: Bitmap? = null

    abstract var visibleName : String
    abstract var damage: Int
    abstract var range: Float
    abstract var attackSpeed: Double
    abstract var description: String
    abstract var cost: Int

    var frames = 10000.0

    companion object {
        var damageMultiplier = 1.0f
        var speedMultiplier = 1.0f
    }

    abstract fun drawAttack(canvas: Canvas)

    override fun draw(canvas: Canvas) {
        if (background != null)
            canvas.drawBitmap(background!!, null, getRect(), null)
        canvas.drawBitmap(image, null, rectHigh, null)
    }

    fun drawRange(canvas: Canvas) {
        val p = Paint()
        p.setARGB(255, 0, 0, 0)
        p.style = Paint.Style.STROKE
        canvas.drawRect(rangeRect, p)
    }

    fun physics(attackers: List<Attacker>) {
        frames += 1.0
        if (frames * attackSpeed * speedMultiplier >= 60) {
            checkRange(attackers)
            if (shooting)
                attack()
        }
        if (shooting && !shotAttacker!!.inGame)
            shooting = false
    }

    override fun setPosition(x: Int, y: Int, width: Int, height: Int) {
        super.setPosition(x, y, width, height)
        val w = GameView.map.mapElementWidth
        val h = GameView.map.mapElementHeight
        rangeRect = Rect(
            (x - range * w).roundToInt(), (y - range * h).roundToInt(),
            (x + width + range * w).roundToInt(), (y + height + range * h).roundToInt()
        )
        rectHigh = Rect(
            x, y - height, x + width, y + height
        )
    }

    abstract fun getInstance() : Tower

    fun setBackground(bitmap : Bitmap?) {
        background = bitmap
    }

    override fun checkRange(attackers: List<Attacker>) {
        shooting = false
        for (mob in attackers)
            if (mob.inGame && rangeRect.contains(mob.getRect())) {
                shooting = true
                shotAttacker = mob
                break
            }
    }
}