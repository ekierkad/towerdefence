package com.example.towerdefence.game

import android.graphics.Canvas
import android.view.SurfaceHolder
import java.lang.Exception

class GameThread(private val surfaceHolder: SurfaceHolder, private val gameView: GameView) : Thread() {

    private var running: Boolean = false
    private val targetFPS = 60
    private var canvas: Canvas? = null

    fun setRunning(isRunning: Boolean) {
        this.running = isRunning
    }

    override fun run() {
        var startTime: Long
        var timeMillis: Long
        var waitTime: Long
        val targetTime = (1000 / targetFPS).toLong()

        while(running) {
            startTime = System.nanoTime()
            canvas = null

            gameView.physics()

            try {
                canvas = surfaceHolder.lockCanvas()
                synchronized(surfaceHolder) {
                    //gameView.draw(canvas)
                    gameView.postInvalidate()

                }
            }
            catch (ex: Exception) {
                ex.printStackTrace()
            }
            finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas)
                    }
                    catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }
            timeMillis = (System.nanoTime() - startTime) / 1000000
            waitTime = targetTime - timeMillis
            try {
                sleep(waitTime)
            }
            catch (ex: Exception) {
                //ex.printStackTrace()
            }
        }
    }
}