package com.example.towerdefence.game.userinterface.gameviewcanvas

import android.graphics.Canvas
import android.view.MotionEvent
import com.example.towerdefence.game.userinterface.ConfirmationButton
import com.example.towerdefence.game.userinterface.upgradebars.BaseShieldIncreaseBar
import com.example.towerdefence.game.userinterface.upgradebars.TowerDamageIncreaseBar

class UpgradeCanvas : GameViewCanvas {

    private var canvasTopLeftX : Int = 0
    private var canvasTopLeftY : Int = 0
    private var canvasWidth : Int = 0
    private var canvasHeight : Int = 0

    private val towerDamageIncreaseBar = TowerDamageIncreaseBar()
    private val baseShieldIncreaseBar = BaseShieldIncreaseBar()
    private val confirmationButton = ConfirmationButton()


    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        canvasTopLeftX = topLeftX
        canvasTopLeftY = topLeftY
        canvasWidth = width
        canvasHeight = height

        towerDamageIncreaseBar.setPosition(canvasTopLeftX, canvasTopLeftY + (canvasHeight * 0.05).toInt(),
            canvasWidth, (canvasHeight * 0.35).toInt())
        baseShieldIncreaseBar.setPosition(canvasTopLeftX, canvasTopLeftY + (canvasHeight * 0.45).toInt(),
            canvasWidth, (canvasHeight * 0.35).toInt())
        confirmationButton.setPosition(canvasTopLeftX + canvasWidth/3,
            canvasTopLeftY + (canvasHeight *0.8).toInt(), canvasWidth/3, (canvasHeight *0.15).toInt())
    }

    override fun draw(canvas: Canvas) {
        towerDamageIncreaseBar.draw(canvas)
        baseShieldIncreaseBar.draw(canvas)
        confirmationButton.draw(canvas)
    }

    override fun isInside(x : Int, y : Int) : Boolean {
        return (x in (canvasTopLeftX .. canvasTopLeftX + canvasWidth) &&
                y in (canvasTopLeftY .. canvasTopLeftY + canvasHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            when {
                towerDamageIncreaseBar.isInside(x, y) -> towerDamageIncreaseBar.onTouch(event)
                baseShieldIncreaseBar.isInside(x, y) -> baseShieldIncreaseBar.onTouch(event)
                confirmationButton.isInside(x, y) -> confirmationButton.onTouch(event)
            }
        }
    }

}