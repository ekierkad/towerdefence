package com.example.towerdefence.game.userinterface.heroes

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import android.view.MotionEvent
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameViewCanvas

abstract class HeroIcon : GameViewCanvas {

    var iconTopLeftX: Int = 0
    var iconTopLeftY: Int = 0
    var iconHeight: Int = 0
    var iconWidth: Int = 0

    protected lateinit var heroBitmap: Bitmap
    protected var iconRect = Rect()
    override fun draw(canvas: Canvas) {
        canvas.drawBitmap(heroBitmap, null, iconRect, null)
    }

    override fun isInside(x: Int, y: Int): Boolean {

        return (x in (iconTopLeftX..iconTopLeftX + iconWidth) &&
                y in (iconTopLeftY..iconTopLeftY + iconHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            if (iconRect.contains(x, y)) {
                heroChosen()
            }
        }
    }

    private fun heroChosen() {
        applyHero()
        GameView.currentCanvas = GameView.CurrentCanvas.MAP
    }

    abstract fun applyHero()

    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        iconTopLeftX = topLeftX
        iconTopLeftY = topLeftY
        iconWidth = width
        iconHeight = height
        iconRect = Rect(iconTopLeftX, iconTopLeftY, iconTopLeftX + iconWidth, iconTopLeftY + iconHeight)
    }


}