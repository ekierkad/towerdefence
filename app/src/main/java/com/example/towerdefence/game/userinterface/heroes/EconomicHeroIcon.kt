package com.example.towerdefence.game.userinterface.heroes

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.heroes.EconomicHero

class EconomicHeroIcon : HeroIcon() {

    private val ECONOMIC_HERO_BITMAP_ID = 403 // potrzebne Icony Bohaterów

    init {
        heroBitmap = ImageResources.getImage(ECONOMIC_HERO_BITMAP_ID)
    }

    override fun applyHero() {
        GameView.player.setHero(EconomicHero())
        GameView.heroPortrait.setHeroBitmap(heroBitmap)
        EconomicHero().setMultipliers()
    }

}