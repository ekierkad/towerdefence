package com.example.towerdefence.game.map

import android.util.Log
import com.example.towerdefence.game.gameobjects.Base
import com.example.towerdefence.game.gameobjects.flatobjects.Brick
import com.example.towerdefence.game.gameobjects.obstacles.Rock
import com.example.towerdefence.game.gameobjects.obstacles.Tree
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameMap
import java.util.*

class RandomMap(
    private val width: Int, private val height: Int,
    private val homeX : Int, private val homeY : Int) {

    private val goStraightProb = 0.65 // Nie wiecej niz ~0.9
    private val minPathLengthMultiplier = 2.0

    private var grid = Array(height) { Array(width) { FieldType.EMPTY} } //tymczasowa mapa na potrzeby algorytmu
    private var prev : Array < Array <Pair> > = Array(height) { Array(width) { Pair(-1, -1) }} // tablica zrodla do DFS-a

    private var stack = Stack<Pair>() //stos do DFS-a

    //generuje mape
    fun createMap() : GameMap {
        do {
            initialize()
            stack.push(Pair(homeX - 1, homeY)) //start przed baza
            prev[homeY][homeX - 1] = Pair(homeX, homeY)
        } while (search() < minPathLengthMultiplier * width) //szukamy sciezki dlugosci przynajmniej 3 * width
        grid[homeY][homeX] = FieldType.HOME
        Log.d("debug", "generated map")
        return getMap()
    }

    private fun putField(x : Int, y : Int, ty : FieldType) {
        if ( x in (0 until width) && y in (0 until height))
            grid[y][x] = ty
    }

    //gemeruje mape z gotowego grid
    private fun getMap() : GameMap {
        val map = GameMap(width, height)
        for (y in 0 until height)
            for (x in 0 until width) {
                when (grid[y][x]) {
                    FieldType.PATH -> map.setField(x, y, Brick())
                    FieldType.HOME -> map.setField(x, y, Base())
                    FieldType.RESERVED, FieldType.EMPTY -> {
                        val t = Math.random()
                        if (t < 0.025)
                            map.setField(x, y, Rock())
                        else if (t < 0.05)
                            map.setField(x, y, Tree())
                    }
                }
            }
        return map
    }

    private enum class FieldType {
        PATH, EMPTY, RESERVED, HOME
    }

    //reprezentacja punktu na planszy
    private class Pair(var x : Int, var y : Int)

    private fun initialize() {
        for (y in 0 until height)
            for (x in 0 until width) {
                grid[y][x] = FieldType.EMPTY
                prev[y][x] = Pair(-1, -1)
            }
        putField(homeX, homeY, FieldType.RESERVED)
        prev[homeY][homeX] = Pair(-2, -2)
        prev[homeY][homeX - 1] = Pair(homeX, homeY)
        grid[homeY][homeX - 1] = FieldType.RESERVED
        putField(homeX + 1, homeY, FieldType.RESERVED)
        putField(homeX, homeY - 1, FieldType.RESERVED)
        putField(homeX + 1, homeY - 1, FieldType.RESERVED)
        putField(homeX, homeY + 1, FieldType.RESERVED)
    }

    private fun put(x : Int, y : Int, prevX : Int, prevY : Int, toStack : Boolean) {
        if (x in 0 until width && y in 0 until height &&
                (grid[y][x] == FieldType.EMPTY ||
                (toStack && grid[y][x] == FieldType.RESERVED &&
                 prev[y][x].x == prev[prevY][prevX].x && prev[y][x].y == prev[prevY][prevX].y))
            ) {

            grid[y][x] = FieldType.RESERVED
            if (toStack)
                stack.push(Pair(x, y))
            prev[y][x] = Pair(prevX, prevY)
        }
    }

    private fun search() : Int {
        while (!stack.empty()) {
            val t = stack.pop()
            if (t.x == 0) {
                var x = t.x
                var y = t.y
                var out = 0
                while (x != -2) {
                    out++
                    grid[y][x] = FieldType.PATH
                    val tmp = prev[y][x]
                    x = tmp.x
                    y = tmp.y
                }
                return out
            }

            val pr = prev[t.y][t.x]
            val px = pr.x
            val py = pr.y

            if (Math.random() < goStraightProb) {
                turn(t.x, t.y, px, py)
                straight(t.x, t.y, px, py)
            } else {
                straight(t.x, t.y, px, py)
                turn(t.x, t.y, px, py)
            }
            put (t.x - 1, t.y - 1, t.x, t.y, false)
            put (t.x + 1, t.y - 1, t.x, t.y, false)
            put (t.x - 1, t.y + 1, t.x, t.y, false)
            put (t.x + 1, t.y + 1, t.x, t.y, false)
        }
        return 0
    }

    private fun straight(x : Int, y : Int, px : Int, py : Int) {
        put(2 * x - px, 2 * y - py, x, y, true)
    }

    private fun turn(x : Int, y : Int, px : Int, py : Int) {
        if (Math.random() > 0.5) {
            put(x - py + y, y + px - x, x, y, true)
            put( x + py - y, y - px + x, x, y, true)
        } else {
            put( x + py - y, y - px + x, x, y, true)
            put(x - py + y, y + px - x, x, y, true)
        }
    }
}