package com.example.towerdefence.game.gameobjects.projectiles

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class GoldProjectile : Projectile() {

    override var id: Int = 307
    override val image: Bitmap = ImageResources.getImage(id)

    init {
        setSpeed(0.1f)
    }

}