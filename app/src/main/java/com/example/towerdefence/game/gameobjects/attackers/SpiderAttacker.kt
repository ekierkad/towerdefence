package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class SpiderAttacker : Attacker() {
    override var id: Int = 112
    override var damage: Int = 25
    override var description: String = "You don't know if it's in your shoe... right now"
    override var health: Int = 125
    override var moveSpeed: Float = 0.03f
    override var reward: Int = 15
    override var visibleName: String = "Spider"

    override val image: Bitmap = ImageResources.getImage(id)
}