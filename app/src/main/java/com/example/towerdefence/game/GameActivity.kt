package com.example.towerdefence.game

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.towerdefence.GameModeActivity
import com.example.towerdefence.R
import kotlinx.android.synthetic.main.activity_game.*
import java.io.InputStream

class GameActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        res = resources
        setContentView(R.layout.activity_game)
        gameView.setParent(this)
        if (intent.hasExtra("Campaign")) {
            gameView.level = intent.getIntExtra("Campaign", 0)
        }
    }

    companion object {

        private lateinit var res: Resources
        val maps = arrayOf(
            R.raw.mission1,
            R.raw.mission2,
            R.raw.mission3,
            R.raw.mission4,
            R.raw.mission5,
            R.raw.mission6,
            R.raw.mission7,
            R.raw.mission8
        )
        val waves = arrayOf(
            R.raw.mission1_wave,
            R.raw.mission2_wave,
            R.raw.mission3_wave,
            R.raw.mission4_wave,
            R.raw.mission5_wave,
            R.raw.mission6_wave,
            R.raw.mission7_wave,
            R.raw.mission8_wave
        )

        fun getMission(i: Int): InputStream {
            return res.openRawResource(maps[i - 1])
        }

        fun getWaves(i: Int): InputStream {
            return res.openRawResource(waves[i - 1])
        }
    }

    fun endGame() {
        val intent = Intent()
        if (GameView.isWon) {
            intent.putExtra("Won", gameView.level)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        else {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }
}