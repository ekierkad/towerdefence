package com.example.towerdefence.game.gameobjects.projectiles

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class ForestProjectile : Projectile() {

    override var id: Int = 308
    override val image: Bitmap = ImageResources.getImage(id)

    init {
        setSpeed(0.1f)
    }
}