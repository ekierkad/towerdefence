package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import kotlin.math.roundToInt
import kotlin.random.Random

class LogicTower : LaserTower() {
    override var id: Int = 213
    override var attackSpeed: Double = 1.0
    override var cost: Int = 3
    override var damage: Int = 3
    override var description: String = "It deals random amount of damage from range [1, 6]"
    override var range: Float = 1f
    override var visibleName: String = "Logic Tower"
    override var laserColor = "yellow"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): LogicTower {
        return LogicTower()
    }

    override fun attack() {
        val dmg = Random.nextInt(1, 7)
        if (shotAttacker!!.getDamage((dmg * damageMultiplier).roundToInt())) {
            shooting = false
            shotAttacker = null
        }
        frames = 0.0
    }
}