package com.example.towerdefence.game.userinterface

import android.graphics.*
import android.view.MotionEvent
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameActivity
import com.example.towerdefence.game.gameobjects.Player

class PlayerStatsBar (private val player: Player) {

    private val GOLD_BITMAP_ID = 901
    private val HP_BITMAP_ID = 902

    private var barTopLeftX : Int = 0
    private var barTopLeftY : Int = 0
    private var barWidth : Int = 0
    private var barHeight : Int = 0
    private var itemWidth : Int = 0
    private var itemHeight : Int = 0

    private val borderWidth : Int = 3

    private val goldImage : Bitmap = ImageResources.getImage(GOLD_BITMAP_ID)
    private val hpImage : Bitmap = ImageResources.getImage(HP_BITMAP_ID)

    fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        barTopLeftX = topLeftX
        barTopLeftY = topLeftY
        barWidth = width
        barHeight = height

        itemWidth = barWidth - 2 * borderWidth
        itemHeight = (barHeight - 2 * borderWidth) / 2

    }

    fun draw(canvas: Canvas) {
        drawRectangle(canvas)
        drawGold(barTopLeftX + borderWidth, barTopLeftY + borderWidth, itemWidth, itemHeight, canvas)
        drawHp(barTopLeftX + borderWidth, barTopLeftY + 2 * borderWidth + itemHeight,
            itemWidth, itemHeight, canvas)
    }

    fun isInside(x : Int, y : Int) : Boolean {
        return (x in (barTopLeftX .. barTopLeftX + barWidth) &&
                y in (barTopLeftY .. barTopLeftY + barHeight))
    }

    fun onTouch(event: MotionEvent) {
    }

    private fun drawRectangle(canvas: Canvas) {
        val paint = Paint()
        paint.color = Color.rgb(96, 96, 96)
        canvas.drawRect(Rect().apply {
            this.set(barTopLeftX, barTopLeftY, barTopLeftX+barWidth, barTopLeftY+barHeight) }, paint)
    }

    private fun drawGold(startX: Int, startY: Int, width: Int, height: Int, canvas: Canvas) {
        canvas.drawBitmap(goldImage, null,
            Rect().apply{this.set(startX, startY, startX + width/2, startY + height)}, null)
        val paint = Paint()
        paint.textSize = 24f
        paint.color = Color.YELLOW
        canvas.drawText(player.getGold().toString(), (startX + width * 0.65).toFloat(), (startY + height/2 + 10).toFloat(), paint)
    }

    private fun drawHp(startX: Int, startY: Int, width: Int, height: Int, canvas: Canvas) {
        canvas.drawBitmap(hpImage, null,
            Rect().apply{this.set(startX, startY, startX + width/2, startY + height)}, null)
        val paint = Paint()
        paint.textSize = 24f
        paint.color = Color.RED
        canvas.drawText(player.getHp().toString(), (startX + width * 0.65).toFloat(), (startY + height/2 + 10).toFloat(), paint)
    }
}
