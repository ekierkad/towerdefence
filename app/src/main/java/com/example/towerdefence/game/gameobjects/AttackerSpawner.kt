package com.example.towerdefence.game.gameobjects
import android.util.Log
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.attackers.Attacker
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameMap

class AttackerSpawner {

    val spawnTime = 100
    var spawnFrame = 100
    var spawned = 0
    var mobToSpawn = 0
    var wave = 0

    private lateinit var waves : List<List<Attacker>>

    fun setWaves(wv : List<List<Attacker>>) {
        waves = wv
        spawned = 0
    }

    fun attackerSpawner(gameMap: GameMap) {
        if (wave < waves.size && spawnTime <= spawnFrame) {
            if (mobToSpawn < waves[wave].size) {
                val mob = waves[wave][mobToSpawn]
                mob.spawnAttacker(gameMap)
                gameMap.spawn(mob)
                mobToSpawn++
            }
            spawnFrame = 0
        } else {
            spawnFrame++
        }
    }

    fun checkEndWave() {
        if (wave < waves.size && mobToSpawn < waves[wave].size)
            return
        if (wave < waves.size)
            nextWave()
        if (wave == waves.size) {
            wave++
            GameView.notifyGameWon()
        }
    }

    private fun nextWave() {
        mobToSpawn = 0
        wave++
        Log.d("waves", "starting wave $wave (if exists)")
        GameView.player.increaseGold(Player.GOLD_PER_WAVE_PER_LEVEL * GameView.player.getGoldPerWaveLevel() + Player.GOLD_PER_WAVE)
    }
}