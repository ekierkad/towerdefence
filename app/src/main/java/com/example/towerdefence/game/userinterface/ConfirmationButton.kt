package com.example.towerdefence.game.userinterface

import android.graphics.Canvas
import android.graphics.Rect
import android.view.MotionEvent
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameViewCanvas

class ConfirmationButton : GameViewCanvas {

    private val CONFIRMATION_IMAGE_ID = 910

    private var canvasTopLeftX : Int = 0
    private var canvasTopLeftY : Int = 0
    private var canvasWidth : Int = 0
    private var canvasHeight : Int = 0
    private val rect = Rect()

    private val image = ImageResources.getImage(CONFIRMATION_IMAGE_ID)

    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        canvasTopLeftX = topLeftX
        canvasTopLeftY = topLeftY
        canvasWidth = width
        canvasHeight = height

        rect.set(topLeftX, topLeftY, topLeftX + width, topLeftY + height)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawBitmap(image, null, getRect(), null)
    }

    override fun isInside(x : Int, y : Int) : Boolean {
        return (x in (canvasTopLeftX .. canvasTopLeftX + canvasWidth) &&
                y in (canvasTopLeftY .. canvasTopLeftY + canvasHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN)
            confirmClicked()
    }

    fun getRect() : Rect {
        return rect
    }

    private fun confirmClicked() {
        GameView.currentCanvas = GameView.CurrentCanvas.MAP
    }
}