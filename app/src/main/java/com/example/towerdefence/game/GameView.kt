package com.example.towerdefence.game

import android.content.Context
import android.graphics.Canvas
import android.graphics.PixelFormat
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.Player
import com.example.towerdefence.game.gameobjects.attackers.Attacker
import com.example.towerdefence.game.gameobjects.towers.Tower
import com.example.towerdefence.game.heroes.WarriorHero
import com.example.towerdefence.game.map.MapGenerator
import com.example.towerdefence.game.userinterface.HeroPortrait
import com.example.towerdefence.game.userinterface.PlayerStatsBar
import com.example.towerdefence.game.userinterface.TopButtonsBar
import com.example.towerdefence.game.userinterface.TowerBar
import com.example.towerdefence.game.userinterface.gameviewcanvas.*

class GameView(context: Context, attrs: AttributeSet): SurfaceView(context, attrs), SurfaceHolder.Callback {

    enum class CurrentCanvas { MAP, ECONOMY, UPGRADE, HERO }

    companion object {
        var selectedTower : Tower? = null
        var player : Player = Player()
        lateinit var map : GameMap
        var economyCanvas = EconomyCanvas()
        var upgradeCanvas = UpgradeCanvas()
        val heroCanvas = HeroCanvas()
        val heroPortrait = HeroPortrait()
        var currentCanvas = CurrentCanvas.HERO
        var isWon: Boolean = false
        var isLost: Boolean = false

        fun notifyGameWon() {
            if (!isWon) {
                isWon = true
            }
        }
    }

    private val topButtonsBar = TopButtonsBar(this)
    private val towerBar = TowerBar()
    private val playerStatsBar = PlayerStatsBar(player)

    private val victoryBitmap = ImageResources.getVictoryImage()
    private val defeatBitmap = ImageResources.getDefeatImage()
    private val endGameRect = Rect()
    private var isGameFinished = false
    private lateinit var parent: GameActivity
    var level: Int = 0

    private var thread: GameThread? = null

    init {
        holder.addCallback(this)
        holder.setFormat(PixelFormat.TRANSPARENT)
        player.setDefaultValues()
        resetMultipliers()
        resetStaticVariables()
    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)
        if (canvas == null)
            return

        getCurrentWidget().draw(canvas)
        heroPortrait.draw(canvas)
        playerStatsBar.draw(canvas)
        towerBar.draw(canvas)
        topButtonsBar.draw(canvas)
        if (selectedTower != null) {
            selectedTower!!.draw(canvas)
            selectedTower!!.drawRange(canvas)
        }
        if (isLost) {
            gameLost(canvas)
        } else if (isWon) {
            gameWon(canvas)
        }
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {

        if (level == 0) {
            map = MapGenerator.getRandomMap(8, 12, 6, 5)
        } else {
            map = MapGenerator.getMissionMap(level)
            MapGenerator.loadMobs(map, level)
        }
        val topBarEndingY = (height * 0.09f).toInt()
        val bottomBarStartingY = (height * 0.85f).toInt()
        player.setDefaultValues()
        heroPortrait.setPosition(0, 0, topBarEndingY, topBarEndingY)
        topButtonsBar.setPosition(topBarEndingY, 0, width - topBarEndingY, topBarEndingY)
        positionMap(topBarEndingY, bottomBarStartingY)
        towerBar.setPosition((0.3*width).toInt(), bottomBarStartingY, (0.7*width).toInt(), height - bottomBarStartingY)
        towerBar.setRandomCheapTowers(4)
        playerStatsBar.setPosition(0, bottomBarStartingY, (0.3*width).toInt(), height - bottomBarStartingY)
        thread = GameThread(holder!!, this)
        thread!!.setRunning(true)
        thread!!.start()
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        thread!!.setRunning(false)
        thread!!.join()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (isGameFinished) {
            parent.endGame()
            return true
        }

        val x = event!!.x.toInt()
        val y = event.y.toInt()
        //Log.d("Touch", "$x $y")
        if (selectedTower != null) {
            val w = selectedTower!!.getRect().width()
            val h = selectedTower!!.getRect().height()
            selectedTower!!.setPosition(x - w / 2, y - h / 2, w, h)
        }

        if (getCurrentWidget().isInside(x, y))
            getCurrentWidget().onTouch(event)
        if (currentCanvas != CurrentCanvas.HERO) {
            if (towerBar.isInside(x, y))
                towerBar.onTouch(event)
            if (topButtonsBar.isInside(x, y))
                topButtonsBar.onTouch(event)
            //Moze kiedys sie przyda
            if (playerStatsBar.isInside(x, y))
                playerStatsBar.onTouch(event)
            if (heroPortrait.isInside(x, y))
                heroPortrait.onTouch(event)
            if (event.action == MotionEvent.ACTION_UP)
                selectedTower = null
        }
        //return super.onTouchEvent(event)
        return true
    }

    fun setRandomTowers() {
        towerBar.setRandomTowers(4)
    }

    fun setParent(gameActivity: GameActivity) {
        this.parent = gameActivity
    }

    fun physics() {
        if (currentCanvas == CurrentCanvas.MAP)
            map.physics()
    }

    private fun gameWon(canvas: Canvas) {
        canvas.drawBitmap(victoryBitmap, null, endGameRect, null)
        isGameFinished = true
        thread!!.setRunning(false)
    }

    private fun gameLost(canvas: Canvas) {
        canvas.drawBitmap(defeatBitmap, null, endGameRect, null)
        isGameFinished = true
        thread!!.setRunning(false)
    }

    private fun resetMultipliers() {
        Tower.damageMultiplier = 1f
        Tower.speedMultiplier = 1f
        Attacker.goldMultiplier = 1f
        Attacker.damageMultiplier = 1f
        Attacker.speedMultiplier = 1f
        Attacker.goldPerMob = 0f
    }

    private fun resetStaticVariables() {
        selectedTower = null
        player.setDefaultValues()
        heroPortrait.setHeroBitmap(null)
        currentCanvas = CurrentCanvas.HERO
        isWon = false
        isLost = false
        economyCanvas = EconomyCanvas()
        upgradeCanvas = UpgradeCanvas()
    }

    private fun positionMap(startY: Int, endY : Int)  {
        val squareSizeX = width / map.getSizeX()
        val squareSizeY = (endY - startY) / map.getSizeY()
        val minSquareSize = Math.min(squareSizeX, squareSizeY)
        val mapWidth = minSquareSize * map.getSizeX()
        val mapHeight = minSquareSize * map.getSizeY()
        map.setPosition((width - mapWidth) / 2, startY + (endY - startY - mapHeight) / 2,
            mapWidth, mapHeight)
        economyCanvas.setPosition((width - mapWidth) / 2, startY + (endY - startY - mapHeight) / 2,
            mapWidth, mapHeight)
        upgradeCanvas.setPosition((width - mapWidth) / 2, startY + (endY - startY - mapHeight) / 2,
            mapWidth, mapHeight)
        heroCanvas.setPosition(
            (width - mapWidth) / 2, startY + (endY - startY - mapHeight) / 2,
            mapWidth, mapHeight
        )
        endGameRect.set((width - mapWidth) / 2, startY + (endY - startY - mapHeight) / 2 + (mapHeight * 0.4).toInt(),
            (width - mapWidth) / 2 + mapWidth, startY + (endY - startY - mapHeight) / 2 + (mapHeight * 0.6).toInt())
    }

    private fun getCurrentWidget() : GameViewCanvas {
        return when (currentCanvas) {
            CurrentCanvas.MAP -> map
            CurrentCanvas.ECONOMY -> economyCanvas
            CurrentCanvas.UPGRADE -> upgradeCanvas
            CurrentCanvas.HERO -> heroCanvas
        }
    }

}