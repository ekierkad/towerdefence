package com.example.towerdefence.game.gameobjects

import android.graphics.*
import com.example.towerdefence.data.ImageResources

class Base : MapDrawable() {
    override var id: Int = 51
    override val image: Bitmap = ImageResources.getImage(id)

    private var background: Bitmap = ImageResources.getDefaultBackground()
    private var rectBig = Rect()

    override fun draw(canvas: Canvas) {
        canvas.drawBitmap(background, null, getRect(), null)
        canvas.drawBitmap(image, null, rectBig, null)
    }

    override fun setPosition(x: Int, y: Int, width: Int, height: Int) {
        super.setPosition(x, y, width, height)
        rectBig.set(x, y - height, x + 2 * width, y + height)
    }
}
