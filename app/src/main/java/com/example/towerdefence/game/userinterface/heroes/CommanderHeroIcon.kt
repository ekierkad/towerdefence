package com.example.towerdefence.game.userinterface.heroes

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.heroes.CommanderHero

class CommanderHeroIcon : HeroIcon() {

    private val COMMANDER_HERO_BITMAP_ID = 402 // potrzebne Icony Bohaterów

    init {
        heroBitmap = ImageResources.getImage(COMMANDER_HERO_BITMAP_ID)
    }

    override fun applyHero() {
        GameView.player.setHero(CommanderHero())
        GameView.heroPortrait.setHeroBitmap(heroBitmap)
        CommanderHero().setMultipliers()
    }

}