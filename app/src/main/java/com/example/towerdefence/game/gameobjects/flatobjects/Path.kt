package com.example.towerdefence.game.gameobjects.flatobjects

import android.graphics.*
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.MapDrawable

class Path : MapDrawable() {
    override var id: Int = 2
    override val image: Bitmap = ImageResources.getImage(0)

    override fun draw(canvas: Canvas) {
        canvas.drawRect(getRect(), Paint().apply { setARGB(255,226,224,145) })
    }

}