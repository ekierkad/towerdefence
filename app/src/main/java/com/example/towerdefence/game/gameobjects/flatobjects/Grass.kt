package com.example.towerdefence.game.gameobjects.flatobjects

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.MapDrawable

class Grass : MapDrawable() {
        override var id: Int = 0
        override val image: Bitmap = ImageResources.getImage(id)
}