package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.*
import android.util.Log
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.MapDrawable
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameMap
import kotlin.math.roundToInt

abstract class Attacker : MapDrawable() {

    private val homeId = 51

    enum class Directions {
        TOP, LEFT, BOTTOM, RIGHT
    }

    private var topLeftX = 0.0f
    private var topLeftY = 0.0f
    private var width = 0f
    private var height = 0f
    var inGame: Boolean = false

    abstract var visibleName : String
    abstract var description : String
    abstract var health : Int
    abstract var reward : Int
    abstract var damage : Int
    abstract var moveSpeed : Float

    private var direction: Directions = Directions.RIGHT
    private var xC: Int = 0
    private var yC: Int = 0

    private var healthH = 0f
    private var healthScale = 0f

    var travelledDistance = 0.0f
    private var distanceOnField = 0.0f

    private val drawRect = RectF()

    private var mapSizeX = 0
    private var mapSizeY = 0

    companion object {
        var goldMultiplier = 1f
        var goldPerMob = 0f
        var damageMultiplier = 1f
        var speedMultiplier = 1f
    }

    fun spawnAttacker(gameMap: GameMap) {
        mapSizeX = gameMap.getSizeX()
        mapSizeY = gameMap.getSizeY()
        for (i in 0 until gameMap.map.size)
            if (gameMap.map[i][0].id == 1) {
                val r = gameMap.map[i][0].getRect()
                yC = i
                setPosition(r.left, r.top, r.width(), r.height())
                break
            }
        inGame = true
    }

    override fun draw(canvas: Canvas) {
        if (inGame) {
            canvas.drawBitmap(image, null, drawRect, null)
            val p = Paint()
            p.setARGB(255, 50, 180, 50)
            canvas.drawRect(
                topLeftX, (topLeftY - (healthH)),
                topLeftX + healthScale * health.toFloat(), topLeftY, p
            )
        }
    }

    override fun setPosition(x: Int, y: Int, width: Int, height: Int) {
        topLeftX = x.toFloat()
        topLeftY = y.toFloat()

        xC = 0
        this.width = width.toFloat()
        this.height = height.toFloat()
        healthScale = width.toFloat() / health.toFloat()
        this.healthH = (height.toFloat() * 1 / 10)
        updateRect()
    }

    fun getPosition() : Point {
        return Point((topLeftX + width/2).roundToInt(), (topLeftY + height/2).roundToInt())
    }

    //fun getReward(): Int {
    //    return reward
    //}
    fun physics(gameMap: GameMap) {
        if (inGame) {
            val modSpeed = moveSpeed * speedMultiplier
            when (direction) {
                Directions.RIGHT -> topLeftX += modSpeed * GameView.map.mapElementWidth
                Directions.TOP -> topLeftY -= modSpeed * GameView.map.mapElementHeight
                Directions.BOTTOM -> topLeftY += modSpeed * GameView.map.mapElementHeight
                Directions.LEFT -> topLeftX -= modSpeed * GameView.map.mapElementWidth
            }

            travelledDistance += modSpeed
            distanceOnField += modSpeed

            if (distanceOnField >= 1.0f) {
                var delta = distanceOnField - 1.0f
                travelledDistance -= delta
                when (direction) {
                    Directions.LEFT -> { xC--; topLeftX += delta * GameView.map.mapElementWidth}
                    Directions.RIGHT -> {xC++; topLeftX -= delta * GameView.map.mapElementWidth}
                    Directions.BOTTOM -> {yC++; topLeftY -= delta * GameView.map.mapElementHeight}
                    Directions.TOP -> {yC--; topLeftY += delta * GameView.map.mapElementHeight}
                }
                distanceOnField = 0.0f
                changeDirections(gameMap)
            }
            updateRect()
        }
    }

    private fun changeDirections(gameMap: GameMap) {
        if (gameMap.map[yC][xC].id == 51) {
            inGame = false
            GameView.player.decreaseHp((damage * damageMultiplier).roundToInt())
        }
        if (direction == Directions.RIGHT || direction == Directions.LEFT) {
            if (yC + 1 < mapSizeY && (gameMap.map[yC + 1][xC].id == 1 || gameMap.map[yC + 1][xC].id == homeId)) {
                direction = Directions.BOTTOM
            } else if (yC - 1 >= 0 && (gameMap.map[yC - 1][xC].id == 1 || gameMap.map[yC - 1][xC].id == homeId)) {
                direction = Directions.TOP
            }
        } else if (direction == Directions.TOP || direction == Directions.BOTTOM) {
            if (xC + 1 < mapSizeX && (gameMap.map[yC][xC + 1].id == 1 || gameMap.map[yC][xC + 1].id == homeId)) {
                direction = Directions.RIGHT
            } else if (xC - 1 >= 0 && (gameMap.map[yC][xC - 1].id == 1 || gameMap.map[yC][xC - 1].id == homeId)) {
                direction = Directions.LEFT
            }
        }

    }

    private fun updateRect() {
        drawRect.set(topLeftX, topLeftY, topLeftX + width, topLeftY + height)
        super.setPosition(
            topLeftX.roundToInt(),
            topLeftY.roundToInt(),
            width.roundToInt(),
            height.roundToInt())
    }

    open fun getDamage(dmg: Int): Boolean {
        health -= dmg
        if (health <= 0) {
            die()
            return true
        }
        return false
    }

    open fun die() {
        if (inGame) {
            inGame = false
            Log.d("die", "$goldPerMob")
            GameView.player.increaseGold((reward * goldMultiplier + goldPerMob).roundToInt())
        }
    }

}
