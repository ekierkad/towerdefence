package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class SnowmanAttacker : Attacker() {
    override var id: Int = 111
    override var damage: Int = 25
    override var description: String = "Jacob, make me some ice-cream"
    override var health: Int = 150
    override var moveSpeed: Float = 0.02f
    override var reward: Int = 15
    override var visibleName: String = "Snowman"

    override val image: Bitmap = ImageResources.getImage(id)
}