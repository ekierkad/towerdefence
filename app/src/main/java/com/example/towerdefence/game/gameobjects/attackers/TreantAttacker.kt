package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class TreantAttacker : Attacker() {
    override var id: Int = 113
    override var damage: Int = 25
    override var description: String = "This treant belongs to pine family"
    override var health: Int = 500
    override var moveSpeed: Float = 0.005f
    override var reward: Int = 20
    override var visibleName: String = "Treant"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getDamage(dmg: Int): Boolean {
        if (dmg < 5)
            return false
        health -= dmg
        if (health <= 0) {
            die()
            return true
        }
        return false
    }
}