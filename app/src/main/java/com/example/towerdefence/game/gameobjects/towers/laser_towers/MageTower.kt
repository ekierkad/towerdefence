package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import com.example.towerdefence.data.ImageResources

class MageTower : LaserTower() {
    override var id: Int = 206
    override var attackSpeed: Double = 5.5
    override var cost: Int = 30
    override var damage: Int = 1
    override var description: String = "Wizard Power"
    override var range: Float = 7f
    override var visibleName: String = "Mage Tower"
    override var laserColor = "violet"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): MageTower {
        return MageTower()
    }

}